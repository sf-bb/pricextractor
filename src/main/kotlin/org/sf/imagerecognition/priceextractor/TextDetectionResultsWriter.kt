package org.sf.imagerecognition.priceextractor

import java.io.File
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit


class TextDetectionResultsWriter(private val results: Collection<TextDetectionResult>) {

    fun writeTo(prefix: String) {
        val dateTime = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).toString().replace("([.]|:|-)".toRegex(), "_")

        val groupedBy = results.groupBy { r -> r.hasText() }

        val withText = groupedBy.getOrElse(true, { listOf() })
        if (!withText.isEmpty()) {
            writeToFile(withText, "${prefix}_${dateTime}_detected.csv", "URL,Text", { r -> "${r.source},${escape(r.text.orEmpty())}," })
        }

        val withErrors = groupedBy.getOrElse(false, { listOf() })
        if (!withErrors.isEmpty()) {
            writeToFile(withErrors, "${prefix}_${dateTime}_errors.csv", "URL,Error", { r -> "${r.source},${escape(r.error.orEmpty())}," })
        }
    }

    private fun writeToFile(responses: List<TextDetectionResult>, filename: String, header: String, value: (r: TextDetectionResult) -> String) {
        val file = File(filename)
        file.printWriter(Charsets.UTF_8).use { out ->
            out.println(header)
            responses.map(value).forEach { out.println(it) }
        }
    }

    private fun escape(text: String): String {
        return "\"${text.replace("\"", "\"\"")}\""
    }
}
