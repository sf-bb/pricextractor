package org.sf.imagerecognition.priceextractor


class TextDetectionResult(
        val source: PhotoSource,
        val text: String?,
        val error: String?) {

    fun hasText(): Boolean {
        return text != null
    }
}