package org.sf.imagerecognition.priceextractor

import java.nio.file.Files
import java.nio.file.Paths


class PhotoGroupsReader(private val fileName: String) {

    fun readPhotoGroups(): Collection<PhotoGroup> {
        val sources = Files.readAllLines(Paths.get(fileName))
                .filter { url -> !url.startsWith("#") }
                .map { url ->
                    if (url.startsWith("file://", true)) {
                        return@map FilePhotoSource(url)
                    } else if (url.startsWith("gs://", true)) {
                        return@map GcsPhotoSource(url)
                    } else if (url.startsWith("https://", true) || url.startsWith("http://", true)) {
                        return@map HttpUrlPhotoSource(url)
                    } else {
                        throw IllegalArgumentException("Unexpected URL protocol $url")
                    }
                }
        println("Number of URLs to process ${sources.size}")
        val splitBy = IndexedPhotoGroup(sources)
                .splitByType()
                .flatMap { group -> group.splitByTotalSizeMB(MAX_TOTAL_SIZE_MB) }
                .flatMap { group -> group.splitBySize(MAX_GROUP_SIZE) }



        println("Split into ${splitBy.size} groups")
        return splitBy
    }

    companion object {
        private const val MAX_GROUP_SIZE = 16
        private const val MAX_TOTAL_SIZE_MB = 8
    }

}