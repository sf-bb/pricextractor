package org.sf.imagerecognition.priceextractor

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking

fun main(args: Array<String>) = runBlocking {

    println("Please provide a path to a file with image URLs in the first args")
    println("Protocols https://, http://, file:// and gs:// are supported")
    println("Don't forget to set 'GOOGLE_APPLICATION_CREDENTIALS' env variable")
    println("Instructions 'https://cloud.google.com/vision/docs/auth#application_default_credentials'")

    println("Limitations: 'https://cloud.google.com/vision/quotas?hl=sk' ")
    println("Max Image size:  4 MB")
    println("Max size per request:  8 MB")
    println("Max number of Images per request:  16")

    val fileName = args[0]


    val groups = PhotoGroupsReader(fileName).readPhotoGroups()

    if (groups.isEmpty()) {
        println("URLs are empty")
        println("Done !")
        System.exit(0)
    }

    val responses = CloudVisionApiClient().use { apiClient ->
        val apiRequests = groups.map { async { apiClient.detectText(it) } }
        return@use apiRequests.map { it.await() }.flatten()
    }

    if (responses.isEmpty()) {
        println("Responses are empty")
        println("Done !")
        System.exit(0)
    }

    TextDetectionResultsWriter(responses).writeTo("results")
    println("Done !")
    System.exit(0)
}

