package org.sf.imagerecognition.priceextractor

import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicLong


interface PhotoGroup {

    fun getPhotos(): Collection<PhotoSource>

    fun splitBySize(size: Int): Collection<PhotoGroup> {
        var index = 0
        val photos = getPhotos()
        if (size <= photos.size) {
            return listOf(this)
        }
        return photos.chunked(size, { sublist -> IndexedPhotoGroup(sublist.toList(), index++) })
    }

    fun splitByType(): Collection<PhotoGroup> {
        val http = mutableListOf<HttpUrlPhotoSource>()
        val file = mutableListOf<FilePhotoSource>()
        val gcs = mutableListOf<GcsPhotoSource>()

        getPhotos().forEach { photo: PhotoSource ->
            when (photo) {
                is HttpUrlPhotoSource -> http.add(photo)
                is FilePhotoSource -> file.add(photo)
                is GcsPhotoSource -> gcs.add(photo)
            }
        }

        return listOf(HttpPhotoGroup(http), GcsPhotoGroup(gcs), FilePhotoGroup(file))
    }

    fun splitByTotalSizeMB(totalSize: Int): Collection<PhotoGroup> {
        return listOf(this)
    }
}


class IndexedPhotoGroup(private val _photos: Collection<PhotoSource>, private val index: Int) : PhotoGroup {

    constructor(photos: Collection<PhotoSource>) : this(photos, 0)

    override fun getPhotos(): Collection<PhotoSource> {
        return _photos
    }

    override fun toString(): String {
        return "Group $index, size ${_photos.size}"
    }
}

class HttpPhotoGroup(private val _photos: Collection<HttpUrlPhotoSource>) : PhotoGroup {

    override fun getPhotos(): Collection<PhotoSource> {
        return _photos
    }

    override fun splitByType(): Collection<PhotoGroup> {
        return listOf(this)
    }

    override fun toString(): String {
        return "Group Http Urls, size ${_photos.size}"
    }

}

class GcsPhotoGroup(private val _photos: Collection<GcsPhotoSource>) : PhotoGroup {

    override fun getPhotos(): Collection<PhotoSource> {
        return _photos
    }

    override fun splitByType(): Collection<PhotoGroup> {
        return listOf(this)
    }

    override fun toString(): String {
        return "Group Gcs Urls, size ${_photos.size}"
    }

}

class FilePhotoGroup(private val _photos: Collection<FilePhotoSource>) : PhotoGroup {

    override fun getPhotos(): Collection<PhotoSource> {
        return _photos
    }

    override fun splitByType(): Collection<PhotoGroup> {
        return listOf(this)
    }

    override fun splitByTotalSizeMB(totalSize: Int): Collection<PhotoGroup> {
        val group = AtomicInteger(0)
        val groupSize = AtomicLong()
        return _photos.groupBy { file ->
            if (groupSize.addAndGet(file.size) <= totalSize * MB) {
                return@groupBy if (group.get() == 0) group.incrementAndGet() else group.get()
            }
            groupSize.set(file.size)
            return@groupBy group.incrementAndGet()
        }.values.map { FilePhotoGroup(it) }
    }

    override fun toString(): String {
        return "Group File Urls, size ${_photos.size}"
    }

    companion object {
        private val MB = 1024 * 1024
    }

}