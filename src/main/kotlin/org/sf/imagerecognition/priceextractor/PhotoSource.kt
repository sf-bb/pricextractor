package org.sf.imagerecognition.priceextractor

import com.google.cloud.vision.v1.AnnotateImageRequest
import com.google.cloud.vision.v1.Feature
import com.google.cloud.vision.v1.Image
import com.google.cloud.vision.v1.ImageSource
import com.google.protobuf.ByteString
import java.io.IOException
import java.net.URL


interface PhotoSource {
    fun toCloudApiRequest(): AnnotateImageRequest
}

class FilePhotoSource(private val url: String) : PhotoSource {

    val size: Long by lazy {
        try {
            val openConnection = URL(url).openConnection()
            val length = openConnection?.getHeaderField("content-length")?.toLong()
            return@lazy length ?: -1L
        } catch (_: IOException) {
            return@lazy -1L
        }
    }

    override fun toCloudApiRequest(): AnnotateImageRequest {
        // Reads the image file into memory
        try {
            val readBytes = URL(url).readBytes()
            val imgBytes = ByteString.copyFrom(readBytes)
            val feat = Feature.newBuilder().setType(Feature.Type.TEXT_DETECTION).build()
            return AnnotateImageRequest.newBuilder()
                    .addFeatures(feat)
                    .setImage(Image.newBuilder().setContent(imgBytes).build())
                    .build()
        } catch (_: IOException) {
            System.err.println("Cannot read file $url")
            val source = ImageSource.newBuilder().setImageUri("Fake url because file does not exist").build()
            val feat = Feature.newBuilder().setType(Feature.Type.TEXT_DETECTION).build()
            return AnnotateImageRequest.newBuilder()
                    .addFeatures(feat)
                    .setImage(Image.newBuilder().setSource(source).build())
                    .build()
        }
    }

    override fun toString(): String {
        return url
    }
}

class HttpUrlPhotoSource(private val url: String) : PhotoSource {
    override fun toCloudApiRequest(): AnnotateImageRequest {
        val imgSource = ImageSource.newBuilder().setImageUri(url).build()
        val img = Image.newBuilder().setSource(imgSource).build()
        val feat = Feature.newBuilder().setType(Feature.Type.TEXT_DETECTION).build()
        return AnnotateImageRequest.newBuilder()
                .addFeatures(feat)
                .setImage(img)
                .build()
    }

    override fun toString(): String {
        return url
    }

}

class GcsPhotoSource(private val gcsPath: String) : PhotoSource {
    override fun toCloudApiRequest(): AnnotateImageRequest {
        val imgSource = ImageSource.newBuilder().setGcsImageUri(gcsPath).build()
        val img = Image.newBuilder().setSource(imgSource).build()
        val feat = Feature.newBuilder().setType(Feature.Type.TEXT_DETECTION).build()
        return AnnotateImageRequest.newBuilder()
                .addFeatures(feat)
                .setImage(img)
                .build()
    }

    override fun toString(): String {
        return gcsPath
    }
}




