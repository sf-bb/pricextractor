package org.sf.imagerecognition.priceextractor

import com.google.cloud.vision.v1.AnnotateImageResponse
import com.google.cloud.vision.v1.ImageAnnotatorClient
import com.google.cloud.vision.v1.ImageAnnotatorSettings

class CloudVisionApiClient : AutoCloseable {


    private val client: ImageAnnotatorClient

    init {
        val settings = ImageAnnotatorSettings.newBuilder().build()
        client = ImageAnnotatorClient.create(settings)
    }

    fun detectText(group: PhotoGroup): Collection<TextDetectionResult> {

        // Builds the image annotation request
        val requests = group.getPhotos().map { source -> source.toCloudApiRequest() }

        val responses = client.batchAnnotateImages(requests).responsesList

        println("$group is completed. Responses ${responses.size}")

        return group.getPhotos().zip(responses).map {
            val error = toError(it.second)
            var text: String? = null
            if (error.isNullOrBlank()) {
                text = toText(it.second)
            }
            return@map TextDetectionResult(it.first, text, error)
        }
    }

    private fun toText(response: AnnotateImageResponse?): String? {
        return response?.textAnnotationsList?.firstOrNull()
                ?.allFields?.filterKeys { field -> field.jsonName == "description" }
                ?.values?.firstOrNull()?.toString()
    }

    private fun toError(response: AnnotateImageResponse?): String? {
        if (response?.hasError() == true) {
            return response.error.message
        }
        return null
    }

    override fun close() {
        client.close()
    }
}